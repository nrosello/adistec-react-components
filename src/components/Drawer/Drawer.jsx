import React from 'react';
import Drawer from 'material-ui/Drawer';
import MenuItem from 'material-ui/MenuItem';
import Divider from 'material-ui/Divider';
import { Link } from 'react-router-dom';
import './Drawer.scss';

const propTypes = {
    items: React.PropTypes.array,
    menuTitle: React.PropTypes.string,
    onClose: React.PropTypes.func,
    open: React.PropTypes.func
};


const AdistecDrawer = ({
    open,
    onClose,
    items,
    menuTitle
}) => (
    <Drawer open={open}
            docked={false}
            onRequestChange={onClose}
            className={open ? 'adistec-drawer open' : 'adistec-drawer closed'}
            >
        <MenuItem primaryText={menuTitle} disabled/>
        {
            items.map((i,k)=>{ return  <Link key={k} to={`/${i.path}`} onClick={onClose}><MenuItem primaryText={i.label}/></Link>; })
        }
        <Divider/>
    </Drawer>
);

AdistecDrawer.propTypes = propTypes;

export default AdistecDrawer;