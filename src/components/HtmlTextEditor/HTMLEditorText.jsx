import React from 'react';
import { connect } from 'react-redux';
import { Field } from 'redux-form';
import ReactQuill from 'react-quill';
import './HTMLEditorText.scss';

function editor ({ input, placeholder }) {
    return (
            <ReactQuill
                {...input}
                modules={quillModules}
                placeholder={placeholder}
                onChange={(newValue, delta, source) => {
                    if (source === 'user') {
                        input.onChange(newValue);
                    }
                }}
                onBlur={(range, source, quill) => {
                    input.onBlur(quill.getHTML());
                }}
            />
    );
}


const quillModules = {
    toolbar: [
        [{ 'header': [1, 2,3,4,5,6, false] }, { 'font': [] },
            'bold', 'italic', 'underline','strike','blockquote',
         { 'color': [] }, { 'background': [] },
        { 'align': [] }, { 'list': 'ordered' }, { 'list': 'bullet' }, { 'indent': '-1' }, { 'indent': '+1' },
        'link', 'clean'
         ]
    ]
};


const propTypes = {
  input: React.PropTypes.object,
  name: React.PropTypes.string,
  placeholder: React.PropTypes.string
};



class AdistecHTMLEditorText extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <div>
                <Field
                    name={this.props.name}
                    className="form-control"
                    component={editor}
                    placeholder={this.props.placeholder}
                    />
            </div>
        );
    }
}

AdistecHTMLEditorText.propTypes = propTypes;


export default connect(null, {

})(AdistecHTMLEditorText);
