import React from 'react';
import AppBar from 'material-ui/AppBar';

const propTypes = {
    title: React.PropTypes.string
};

const AdistecAppBar = (title) => (
	  	<AppBar
	    	title={title}
	    	iconClassNameRight="muidocs-icon-navigation-expand-more"
	  	/>
);

AdistecAppBar.propTypes = propTypes;

export default AdistecAppBar;