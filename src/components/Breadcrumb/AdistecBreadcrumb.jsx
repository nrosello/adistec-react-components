import React from 'react';
import { Breadcrumb, BreadcrumbItem } from 'react-bootstrap';
import './Breadcrumb.scss';
import styled from 'styled-components';
import find from 'lodash/find';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { matchPath } from 'react-router';

const propTypes = {
    /**
     * URL actual
     */
    currentPath: PropTypes.string.isRequired,
    /**
     * Array de objetos. Debe ser del tipo [{label: string, path: string, notRedirect: boolean, customLabel: function}]
     * donde path es la ruta del objeto, label es el nombre del objeto, notRedirect especifica si el objeto es redireccionable o no y customLabel es una funcion
     * para obtener el label de los pathVariables pasado por parametros de la URL a resolver
     */
    routes: PropTypes.array.isRequired,
    /**
     * Simbolo para separar una ruta de otra
     */
    separator:PropTypes.string
};

const Breadcrumbs = styled(Breadcrumb)`
   .breadcrumbs-container .breadcrumb > li + li:before {
       content: ${props=>props.styles.separator} !important;
    }
`;


class AdistecBreadcrumb extends React.Component {
    constructor(props) {
        super(props);
    }


    render() {
        let currentPathItems = this.props.currentPath === '/' ? [''] : this.props.currentPath.split('/');
        let fullPath = '/';
        return (
            <div className="breadcrumbs-container">
                <div className="container flex-space-between flex-center">
                    <Breadcrumbs styles={ { separator: this.props.separator } }>
                        {currentPathItems.map((path, index) => {
                            fullPath += index > 1 ? '/' + path : path;
                            let item = find(this.props.routes, function(route) {return matchPath(fullPath, route.path) && matchPath(fullPath, route.path).isExact;});
                            let label = item && item.label? item.label : (item.customLabel ? item.customLabel(matchPath(fullPath, item.path).params) : path);
                            return (<Breadcrumb.Item key={index} active as={'div'}>
                                {item?
                                    item.notRedirect || index === (currentPathItems.length -1) ? //Se muestra como Read Only. No se puede redirigir
                                        <span> {label} </span>
                                        : <Link to={fullPath}> {label} </Link>
                                    :  <span> {label} </span>}
                            </Breadcrumb.Item>);
                        })
                        }
                    </Breadcrumbs>
                </div>
            </div>
        );
    }
}

AdistecBreadcrumb.propTypes = propTypes;


export default AdistecBreadcrumb;


