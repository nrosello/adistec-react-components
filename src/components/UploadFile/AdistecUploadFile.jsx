import React from 'react';
import { connect } from 'react-redux';
import { change, Field, untouch } from 'redux-form';
import DropZoneInput from './DropZoneInput.jsx';
import './AdistecUploadFile.scss';


const propTypes = {
    afterDrop: React.PropTypes.func,
    allowedFileTypes: React.PropTypes.string, //Promise, return undefined on validation ok or error message on validation error;
    directory: React.PropTypes.string,
    hint: React.PropTypes.string,
    label: React.PropTypes.string,
    maxSize: React.PropTypes.number, //Must be bytes format
    multiple: React.PropTypes.boolean,
    name: React.PropTypes.string.isRequired,
    showDownloadButton: React.PropTypes.boolean,
    validate: React.PropTypes.array
};


class AdistecUploadFile extends React.Component {
    constructor(props) {
        super(props);
    }

    untouch = () => {
        this.props.untouchSelectField(this.refs[this.props.name].context._reduxForm.form, this.props.name);
    }

    render() {
        return (
            <div className="file-upload-container">
                <Field
                    {...this.props}
                    multiple={this.props.multiple}
                    name={this.props.name}
                    className="form-control"
                    component={DropZoneInput}
                    validate={this.props.validate}
                    withRef
                    ref={this.props.name}
                    label={this.props.label}
                    hint={this.props.hint}
                    allowedFileTypes={this.props.allowedFileTypes}
                    maxSize={this.props.maxSize}
                    showDownloadButton={this.props.showDownloadButton}
                    directory={this.props.directory}
                    icon={this.props.icon}
                    untouch={this.untouch.bind(this)}
                    afterDrop={this.props.afterDrop}
                />
            </div>
        );
    }
}

AdistecUploadFile.propTypes = propTypes;

export default connect(null, {
    untouchSelectField: (formName,fieldName) => untouch(formName,fieldName)
})(AdistecUploadFile);