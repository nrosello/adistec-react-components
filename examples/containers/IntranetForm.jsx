import React from 'react';
import { reduxForm , validate, asyncValidate, Field, getFormValues , getFormSyncErrors ,submit, touch,change,isValid,stopSubmit,reset } from 'redux-form';
import AdistecTextField from '../../src/components/TextField';
import AdistecButton from '../../src/components/Button';
import AdistecUploadFile from '../../src/components/UploadFile';
import SingleSelectionList from '../../src/components/SingleSelectionList';
import Nav from '../../src/components/Nav';
import { FormattedMessage } from 'react-intl';
import GeneralValidations from '../../src/components/GeneralValidations';


const imageValidation = (value) => new Promise(function(resolve){
  let image = new Image();
  image.src = value[0].preview;
  image.onload = function() {
     return this.width == 1280 && this.height == 200 ? resolve(null) : resolve('adasdfasdfd');  
  };
});

class IntranetForm extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            showSuccessRegisterMessage: false,
            registerMessage: null
        };
    }

    getSexList = () => {return [{ label: 'Male', value:1 },{ label: 'Female',value:2 }]; };

    getSelectionListItems = () => {
        return[
            { id:'1', name:'Internet', icon:<i className="material-icons">wifi</i> },
            { id:'2', name:'NetSuite', icon: <span className="a-ns-logo">NS</span> },
            { id:'3', name:'Telefonía / Telepresencia', icon:<i className="material-icons">phone</i> },
            { id:'4', name:'Adistec Portal', icon:<i className="material-icons">phone</i> },
            { id:'5', name:'Computadora', icon:<i className="material-icons">computer</i> },
            { id:'6', name:'Website / Intranet', icon:<i className="material-icons">web</i> },
            { id:'7', name:'Impresora', icon:<i className="material-icons">print</i> },
            { id:'8', name:'Adistec University', icon:<i className="material-icons">school</i> },
            { id:'9', name:'Email / Outlook', icon:<i className="material-icons">email</i> },
            { id:'10', name:'Ecommerce', icon:<i className="material-icons">shopping_cart</i> },
            { id:'11', name:'OneDrive', icon:<i className="material-icons">cloud</i> },
            { id:'12', name:'AEC', icon:<i className="material-icons">add</i> }];
    };

    handleSubmit(data) {
        this.setState({ showSuccessRegisterMessage: true,
                      registerMessage:
                          <div className="row" style={{ marginLeft: '10px', marginTop: '15px' }}>
                              <div className="col s6">
                                  <label><strong>First Name</strong></label>
                                  <p>{data.firstName}</p>
                              </div>
                              <div className="col s6">
                                  <label><strong>Last Name</strong></label>
                                  <p>{data.lastName}</p>
                              </div>
                              <div className="col s6">
                                  <label><strong>Email</strong></label>
                                  <p>{data.email}</p>
                              </div>
                              <div className="col s6">
                                  <label><strong>Sex</strong></label>
                                  <p>{this.getSexList().find(s => s.value==data.sex).label}</p>
                              </div>
                              <div className="col s6">
                                  <label><strong>Activate</strong></label>
                                  <p>{data.activate? 'Yes': 'No'}</p>
                              </div>
                              <div className="col s6">
                                  <label><strong>Comments</strong></label>
                                  <p>{data.comments? data.comments : '-'}</p>
                              </div>
                          </div>
               });
    }

    closeMessage = () => {
        this.setState({ showSuccessRegisterMessage: false });
    }

    render(){
        return (
            <div className="a-theme_modern">
                <div className="a-section_intro a-gradient_blue a-padding_30 a-padding--bottom_lg a-padding--top_0">
                    <div className="container">
                        <Nav />
                        <h2 className="center-align a-margin--bottom_0">¿Cómo te podemos ayudar?</h2>
                        <h4 className="center-align">Selecciona la opción que mejor describa tu problema</h4>
                    </div>
                </div>
                <div className="a-solid_lightblue a-section_main">
                    <div className="container a-move--moveup_md">
                        <div className="col s12 m12">
                            <SingleSelectionList list={this.getSelectionListItems()}
                                                 validate={GeneralValidations.required}
                                                 name="eventStatus"
                                                 columns={2}
                            />
                        </div>
                        <div className="col s12 m12">
                            <h4 className="center-align">Describe tu problema lo mejor que puedas</h4>
                            <div className="a-card">
                                <AdistecTextField name="shortDescription"
                                                  size="12"
                                                  multiLine
                                                  rows={1}
                                                  label={<FormattedMessage id="intranetForm.textareaPlaceholder"/>}
                                                  className="adistec-textarea"
                                                  validate={GeneralValidations.required}
                                                  clearButton
                                />
                            </div>
                        </div>
                        <div className="col s12 m12 a-margin--top_md">
                            <h4 className="center-align">Adjunta cualquier archivo que consideres necesario</h4>
                            <AdistecUploadFile name="designRequestFileAndLink"
                                               size="12 m12 l4"
                                               afterDrop={imageValidation}
                                               validate={GeneralValidations.required}
                                               multiple
                                               showContent
                                               allowedFileTypes=".pdf, .zip, .tar, .jpg, .png, .gif"
                                               maxSize={6000000}
                                               label={'Drag and drop your files here'}
                                               icon={<i className="material-icons">backup</i>}
                            />
                        </div>
                        <div className="bottom-buttons-container center a-margin--top_md">
                            <AdistecButton type="submit"
                                           label="Crear ticket"
                                           className="submit pill large"
                                           width={'auto'}
                            />
                        </div>
                    </div>
                </div>
            </div>
        );

    }
}

IntranetForm = reduxForm({
    form:'intranetForm',
    enableReinitialize: true,
    validate,
    asyncValidate
})(IntranetForm);

export default IntranetForm;